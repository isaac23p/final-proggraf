import React from 'react';
import { useForm } from 'react-hook-form';

export default function App() {
  const { register, handleSubmit, errors } = useForm();
  const onSubmit = data => console.log(data);
  console.log(errors);
  
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <input type="text" placeholder="Nombre del Juego" name="Nombre del Juego" ref={register} />
      <input type="number" placeholder="Año que salio" name="Año que salio" ref={register} />
      <input type="text" placeholder="Plataforma Preferida" name="Plataforma Preferida" ref={register} />
      <input type="text" placeholder="Tipo de Juego" name="Tipo de Juego" ref={register} />

      <input type="submit" />
    </form>
  );
}